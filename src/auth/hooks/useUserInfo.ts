import storage from "../../core/helpers/storage";

export function useUserInfo() {
  return { data: storage.getItem("userInfo") };
}
