import storage from "../../core/helpers/storage";
import { useNavigate } from "react-router-dom";

export function useLogout() {
  const navigate = useNavigate();
  const logout = () => {
    storage.removeToken();
    storage.removeItem("userInfo")
    navigate("/login");
  };

  return { logout };
}
