import { useMutation } from "react-query";

import { apiLogin } from "../../core/api/auth";
import storage from "../../core/helpers/storage";

const login = async ({
  email,
  password,
}: {
  email: string;
  password: string;
}): Promise<any> => {
  const result: any = await apiLogin({ email, password });
  if (!result?.data) return result;
  storage.setToken(result?.data?.token);
  storage.setItem("userInfo", result?.data?.user);
  return result?.data;
};

export function useLogin() {
  const { isLoading, mutateAsync } = useMutation(login);

  return { isLoggingIn: isLoading, login: mutateAsync };
}
