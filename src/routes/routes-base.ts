import React from "react";
const Login = React.lazy(() => import("../auth/pages/Login"));
import ListClass from "page/ListClass";
import NotFound from "page/NotFound";
import ManagerUsers from "page/ManagerUsers";
import DetailClass from "page/DetailClass";
import { images } from "core/assets";

const routes: any[] = [
  {
    path: "/login",
    element: Login,
    private: false,
  },
  {
    path: "/list-class",
    name: "Danh sách class",
    icon: images.iconListClass,
    element: ListClass,
    private: true,
    roles: [0, 1],
    subPath: [
      {
        path: "/detail/:id",
        element: DetailClass,
        private: true,
      },
    ],
  },
  {
    path: "/manager-users",
    name: "Quản lý users",
    icon: images.iconListUser,
    element: ManagerUsers,
    private: true,
    roles: [0],
  },

  // {
  //   path: "/trash",
  //   name: "Thùng rác",
  //   icon: images.trash,
  //   element: ListClass,
  //   private: true,
  //   roles: [0, 1],
  // },
  {
    path: "*",
    element: NotFound,
    private: false,
  },
];

export default routes;
