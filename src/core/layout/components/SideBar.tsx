import React, { useEffect, useState } from "react";
import { Layout, Menu } from "antd";
import type { MenuProps } from "antd";
type MenuItem = Required<MenuProps>["items"][number];

import styles from "./styles.module.scss";
import { useLocation, useNavigate } from "react-router-dom";
import { useUserInfo } from "auth/hooks/useUserInfo";
import styled from "styled-components";
import routes from "routes/routes-base";
import { useAuth } from "auth/contexts/AuthProvider";

const { Sider } = Layout;

const TitleHello = styled.h2`
  color: white;
  font-weight: 600;
`;

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[]
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

const SideBar = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const { hasRole } = useAuth();
  const { data: userInfo }: any = useUserInfo();
  const [items, setItems] = useState<MenuItem[]>([]);

  useEffect(() => {
    setItems(
      routes
        .filter((route) => route.private && hasRole(route.roles))
        .map((route) => {
          return getItem(route.name, route.path, <img src={route.icon} />);
        })
    );
  }, []);

  return (
    <div className={styles.sidebar}>
      <Sider>
        {userInfo && (
          <div className="w-100 d-flex-c-c mb-10">
            <TitleHello>Hello {userInfo.name}!</TitleHello>
          </div>
        )}
        <Menu
          theme="dark"
          defaultSelectedKeys={["/" + pathname.split("/")[1]]}
          mode="inline"
          items={items}
          onClick={({ item, key }) => navigate(key)}
        />
      </Sider>
    </div>
  );
};

export default SideBar;
