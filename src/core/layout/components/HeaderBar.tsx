import { Link, useLocation } from "react-router-dom";
import { images } from "../../assets";
import styled from "styled-components";
import routes from "routes/routes-base";
import { useAuth } from "auth/contexts/AuthProvider";

import { Avatar, TextField } from "@mui/material";
import { Button, Modal } from "antd";

import { ExclamationCircleOutlined } from "@ant-design/icons";
import CustomAutocomplete from "core/components/ReactHookForm/Autocomplete";
import { useForm } from "react-hook-form";
import styles from "./styles.module.scss";
import { useState } from "react";
import ModalConfirm from "core/components/ModalConfirm";

const HeaderContainer = styled.div`
  height: 60px;
  background-color: #222433;
  display: flex;
  position: relative;
  box-shadow: 0px 5px 25px -5px rgba(254, 166, 40, 0.5);
  -webkit-box-shadow: 0px 5px 25px -5px rgba(254, 166, 40, 0.5);
  z-index: 10;
`;

const Logo = styled.div`
  background-image: url(${images.logo});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  cursor: pointer;
  width: 40px;
  height: 40px;
  margin: auto 15px;
`;

const BrandName = styled.span`
  font-size: 24px;
  font-weight: bold;
  color: white;
  cursor: pointer;
  align-items: center;
  display: flex;
  padding-right: 60px;
`;

const Logout = styled.div`
  background-image: url(${images.logoutIcon});
  height: 30px;
  width: 30px;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  cursor: pointer;
  margin-right: 30px;
  margin-left: 20px;
`;

const OtherInfo = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  height: 99%;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
`;

const NavItem = styled(({ isActive, ...props }) => (
  <StyledLink to={props.path}>
    <div {...props}>
      <img src={props.icon} className="mr-10" />
      {props.name}
    </div>
  </StyledLink>
))`
  color: #b6b6a6;
  height: 100%;
  display: flex;
  align-items: center;
  font-size: 16px;
  font-weight: 500;
  padding-right: 25px;
  padding-left: 20px;
  border-left: 2px solid hsla(0, 0%, 100%, 0.1);
  border-right: 2px solid hsla(0, 0%, 100%, 0.1);
  filter: ${(props) =>
    props.isActive
      ? "brightness(0) saturate(100%) invert(76%) sepia(53%) saturate(2057%) hue-rotate(338deg) brightness(101%) contrast(99%)"
      : ""};
  border-bottom: ${(props) =>
    props.isActive ? "3px solid #EA5C5C" : "3px solid transparent"};
  &:hover {
    color: white;
    background-color: hsla(0, 0%, 100%, 0.1);
    border-bottom: 3px solid #ea5c5c;
  }
`;

const HeaderBar = () => {
  const { logout, userInfo, hasRole } = useAuth();
  const { pathname } = useLocation();

  const [isOpenModalLogout, setIsOpenModalLogout] = useState(false);

  return (
    <HeaderContainer>
      <Logo />
      <BrandName>MyProject</BrandName>
      {routes.map((route, index) => {
        if (route?.private && hasRole(route.roles)) {
          return (
            <NavItem
              key={index}
              icon={route.icon}
              path={route.path}
              name={route.name}
              isActive={"/" + pathname.split("/")[1] === route.path}
            />
          );
        }
      })}

      <OtherInfo>
        <Avatar src={userInfo?.avatar} />
        <Logout onClick={() => setIsOpenModalLogout(true)} />
      </OtherInfo>
      <ModalConfirm
        isOpen={isOpenModalLogout}
        onCancel={() => setIsOpenModalLogout(false)}
        content="Bạn có chắc chắn muốn đăng xuất?"
        footer={
          <div className="d-flex-sr-c">
            <Button
              className="w-100 rounded-7"
              onClick={() => setIsOpenModalLogout(false)}
            >
              Hủy
            </Button>
            <Button className="w-100 rounded-7" type="primary" onClick={logout}>
              Đăng xuất
            </Button>
          </div>
        }
      />
    </HeaderContainer>
  );
};

export default HeaderBar;
