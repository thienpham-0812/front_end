import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Modal } from "antd";
import styled from "styled-components";

const FlexContainer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
`;

interface IProps {
  isOpen: boolean;
  onCancel: any;
  footer: any;
  content: any;
}

const ModalConfirm = (props: IProps) => {
  const { isOpen, onCancel, footer, content } = props;
  return (
    <Modal
      open={isOpen}
      onCancel={onCancel}
      footer={footer}
      centered
      className="modal-confirm-custom"
    >
      <FlexContainer>
        <div>
          <ExclamationCircleOutlined />
        </div>
        <div className="ml-50">{content}</div>
      </FlexContainer>
    </Modal>
  );
};

export default ModalConfirm;
