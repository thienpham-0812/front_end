import logo from "./images/logoUET.png";
import loginBg from "./images/loginBg.svg";
import notFound from "./images/notFound.svg";
import logoutIcon from "./images/logoutIcon.svg";
import addIcon from "./images/add-icon.svg";
import searchIcon from "./images/search-icon.svg";
import refreshIcon from "./images/refresh-icon.svg";
import arrowBack from "./images/arrow-back.svg";
import infoLabelIcon from "./images/info-label-icon.svg";
import editIcon from "./images/edit-icon.svg";
import iconCheckedCircle from "./images/iconCheckedCircle.svg";
import iconListClass from "./images/icon-list-class.svg";
import iconListUser from "./images/icon-list-user.svg";
import trash from "./images/trash.svg";
import gridViewIcon from "./images/grid-view-icon.svg";
import uploadImage from "./images/upload-img.svg";
import downloadImage from "./images/download-img.svg";
import spin from "./images/spin.svg";
import cursor from "./images/cursor.svg";
import crop from "./images/crop.svg";
import eraser from "./images/eraser.svg";
import rotate from "./images/rotate.svg";
import zoomOut from "./images/zoomOut.svg";
import zoomIn from "./images/zoomIn.svg";
import saveImage from "./images/saveImage.svg";
import saveAsImage from "./images/saveAsImage.svg";
import circleSwitch from "./images/circle-switch.svg";
import rectangleSwitch from "./images/rectangle-switch.svg";
import emptyBg from "./images/empty_bg.jpg";
import autoRemoveBg from "./images/auto-remove-bg.svg";

export const images = {
  logo,
  loginBg,
  notFound,
  logoutIcon,
  addIcon,
  searchIcon,
  refreshIcon,
  arrowBack,
  infoLabelIcon,
  editIcon,
  iconCheckedCircle,
  iconListClass,
  iconListUser,
  trash,
  gridViewIcon,
  uploadImage,
  downloadImage,
  spin,
  cursor,
  crop,
  eraser,
  rotate,
  zoomOut,
  zoomIn,
  saveImage,
  saveAsImage,
  circleSwitch,
  rectangleSwitch,
  emptyBg,
  autoRemoveBg,
};

export const colors = {
  mainColor: "#fea628",
  mainColorRGB: "#fea62850",
  mainColorHover: "#fea62880",
  neutralHeaderColor: "#222433",
  textHover: "rgba(222, 20, 29, 0.5)",
  newColor: "#0057ff",
  bigImageColor: "#e74c3c",
  haveImageColor: "#2ecc71",
  titleForm: "#5a6882",
  contentForm: "#222433",
  borderDividerColor: "#e7e7e7",
};
