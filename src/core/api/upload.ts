import { sendPost } from "./axios";

export const uploadImage = (file: File) => {
  const formData = new FormData();
  formData.append("files", file);
  return sendPost("/upload", formData);
};

export const apiUploadListImage = (payload: {
  image: File;
  name: string;
  description: string;
  class_id: any;
}) => {
  const formData = new FormData();
  formData.append("image", payload.image);
  formData.append("name", payload.name);
  formData.append("description", payload.description);
  formData.append("class_id", payload.class_id);
  return sendPost("class/upload-image", formData);
};
