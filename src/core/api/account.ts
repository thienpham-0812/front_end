import { sendDelete, sendGet, sendPost, sendPut } from "./axios";

export const getListUser = (params: { key_word: string | undefined }) =>
  sendGet("/user", params);
export const updateUserStatus = (id: number, payload: any) =>
  sendPut(`/users/${id}/update-status`, payload);
  export const deleteUserById = (id?: number) => sendDelete(`/user/${id}`)
export const createAccount = (payload: any) => sendPost("/user", payload);
export const updateAccount = (id: number, payload: any) =>
  sendPut(`/user/${id}`, payload);
export const changePassword = (id: number, payload: any) =>
  sendPut(`users/${id}/change-password`, payload);
