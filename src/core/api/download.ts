import Axios from "axios";

import configs from "../config";
import { ErrorCode } from "../constants/enums";
import { handleErrorMessage } from "../helpers";
import storage from "../helpers/storage";

const axiosInstance = Axios.create({
  baseURL: configs.API_DOMAIN,
  responseType: "blob",
});
axiosInstance.interceptors.request.use(
  (config: any) => {
    const token = storage.getToken();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error: any) => Promise.reject(error)
);

axiosInstance.interceptors.response.use(
  (response: any) => response,
  async (error: any) => {
    const originalConfig = error.config;
    if (error.response?.data?.errorCode !== ErrorCode.UNAUTHORIZED) {
      handleErrorMessage(error);
      return Promise.reject(error);
    }
    return Axios.post(`${configs.API_DOMAIN}/auth/refresh-token`, {
      refreshToken: storage.getRefreshToken(),
    })
      .then((res: any) => {
        if (res?.data?.data?.token) {
          const data = res.data.data;
          storage.setToken(data.token);
          originalConfig.headers.Authorization = `Bearer ${data.token}`;
          return Axios(originalConfig);
        }
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  }
);

const sendPostDownload = (url: string, params?: any, queryParams?: any) =>
  axiosInstance
    .post(url, params, { params: queryParams })
    .then((res) => res.data);

export const apiDownloadListClass = (id?: number[]) =>
  sendPostDownload("/class/download", { id });

export const apiGetDataImagePicked = (path: string) =>
  sendPostDownload("/class/download-image", { image: path });
