import { sendGet, sendPost } from "./axios";

export const apiCreateClass = (payload: any) =>
  sendPost("/class/create", payload);
export const apiEditClass = (id: any, payload: any) =>
  sendPost(`/class/edit/${id}`, payload);

export const apiGetListClass = (params: { key_word: string | undefined }) =>
  sendGet("/class", params);

export const apiGetDetailClass = (id: string, payload: any) =>
  sendGet(`/class/detail/${id}`, payload);

export const apiDeleteClass = (id: number | number[]) =>
  sendPost(`/class/delete/`, { id });

export const apiDeleteImage = (path: string | string[]) =>
  sendPost("/class/delete-image", { file_name: path });
