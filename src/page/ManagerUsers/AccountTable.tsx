import { message, Switch, Table, Popover, Button, Modal } from "antd";
import type { ColumnsType } from "antd/es/table";
import { useMutation, useQueryClient } from "react-query";
import {
  EditOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";

import { IAccount, IAccountForm } from "core/constants/interfaces";
import { getIndexTable } from "core/helpers";
import { ActionType, UserStatus } from "core/constants/enums";
import { deleteUserById, updateUserStatus } from "core/api/account";
import { useState } from "react";
import AccountModal from "./AccountModal";
import moment from "moment";
import { Grid, InputAdornment, TextField } from "@mui/material";
import styled from "styled-components";
import { images } from "core/assets";
import styles from "./styles.module.scss"

const TotalClassTitle = styled(Grid)`
  font-size: 24px;
  font-weight: 500;
`;
const Total = styled.span`
  font-size: 32px;
  font-weight: 700;
`;

interface IProps {
  data: IAccount[];
  loading: boolean;
  pageIndex: number;
  pageSize: number;
  handleSearch: any;
}

export default function AccountTable({
  data,
  loading,
  pageIndex,
  pageSize,
  handleSearch,
}: IProps) {
  const queryClient = useQueryClient();

  const [open, setOpen] = useState(false);
  const [currentData, setCurrentData] = useState<IAccount>();
  const [isOpenModalCreateAccount, setOpenModalCreateAccount] = useState(false);

  const { mutate: deleteUser, isLoading: loadingDeleteUser } = useMutation(
    (record: any) => deleteUserById(record?.id),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("accounts");
        message.destroy();
        message.success("Xóa thành công");
      },
    }
  );

  const columns: ColumnsType<IAccount> = [
    {
      title: "STT",
      dataIndex: "index",
      key: "index",
      render: (value, record, index) => (
        <div>{getIndexTable(pageIndex, pageSize, index)}</div>
      ),
      align: "center",
    },
    {
      title: "Họ tên",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",
      key: "created_at",
      align: "center",
      render: (value, record, index) => (
        <div>{moment(record?.created_at).format("DD/MM/YYYY HH:mm")}</div>
      ),
    },
    {
      title: "",
      align: "center",
      render: (value, record, index) => (
        <div className="d-flex-c-c">
          <Popover content={"edit info"} placement="bottom">
            <span
              className="icon-action mr-10"
              onClick={() => {
                setCurrentData(record);
                setOpen(true);
              }}
            >
              <EditOutlined />
            </span>
          </Popover>
          <Popover content={"delete user"} placement="bottom">
            <span
              className="icon-action"
              onClick={() => {
                showConfirmDeleteUser(record);
              }}
            >
              <DeleteOutlined />
            </span>
          </Popover>
        </div>
      ),
    },
  ];

  const showConfirmDeleteUser = (record: any) => {
    Modal.confirm({
      title: "Bạn có chắc chắn muốn xóa " + record.name + " khỏi hệ thống?",
      icon: <ExclamationCircleOutlined />,
      onOk() {
        return deleteUser(record);
      },
      centered: true,
      className: "modal-delete-custom"
    });
  };

  return (
    <>
      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        direction="row"
        pb={2}
        mb={3}
        borderBottom={2}
        borderColor="gray"
      >
        <TotalClassTitle item>
          Tổng số users: <Total>{data?.length}</Total>
        </TotalClassTitle>
        <Grid item direction="row" display="flex" alignItems="center">
          <TextField
            placeholder="Tìm kiếm ..."
            className={styles.searchBar}
            onChange={handleSearch.keywordSearch}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <img src={images.searchIcon} />
                </InputAdornment>
              ),
            }}
          />
          <Button
            className="btn btn--create ml-20"
            onClick={() => setOpenModalCreateAccount(true)}
          >
            Tạo tài khoản +
          </Button>
        </Grid>
      </Grid>
      <Table
        columns={columns}
        dataSource={data}
        bordered
        pagination={false}
        className="tableCus"
        rowKey={(obj) => obj.id}
        loading={loading}
      />
      {open && (
        <AccountModal
          type={ActionType.UPDATE}
          open={open}
          onCancel={() => {
            setOpen(false);
          }}
          data={currentData}
        />
      )}
      {isOpenModalCreateAccount && (
        <AccountModal
          type={ActionType.CREATE}
          open={isOpenModalCreateAccount}
          onCancel={() => setOpenModalCreateAccount(false)}
        />
      )}
    </>
  );
}
