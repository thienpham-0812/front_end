import { Button } from "antd";
import { useQuery } from "react-query";
import _ from "lodash";
import { useState } from "react";

import { images } from "core/assets/";
import useFilter from "core/hooks/common/useFilter";
import { IFilter } from "core/constants/interfaces";
import PaginationCustom from "core/components/PaginationCustom";
import AccountTable from "./AccountTable";
import { getListUser } from "core/api/account";
import { formatText } from "core/helpers";
import AccountModal from "./AccountModal";
import { ActionType, UserStatus } from "core/constants/enums";

const defaultFilter: IFilter = {
  // pageIndex: 1,
  // pageSize: 10,
};

export default function Account() {
  const {
    filter,
    handleFilterChange,
    resetFilter,
    handlePageChange,
    handleRowsPerPageChange,
    handleSearch,
  } = useFilter(defaultFilter);
  const [open, setOpen] = useState(false);

  const { data: accounts, isLoading: loadingAccounts } = useQuery(
    ["accounts", filter],
    () => getListUser({ key_word: filter.keyword }),
    { keepPreviousData: true }
  );

  return (
    <div className="page-container">
      <div className="table-container">
        <AccountTable
          data={accounts?.data || []}
          loading={loadingAccounts}
          pageIndex={filter.pageIndex || 1}
          pageSize={filter.pageSize || 10}
          handleSearch={handleSearch}
        />
        {/* <PaginationCustom
          total={accounts?.data?.length ?? 0}
          pageIndex={filter.pageIndex || 1}
          pageSize={filter.pageSize || 10}
          onPageChange={handlePageChange}
          onRowsPerPageChange={handleRowsPerPageChange}
        /> */}
      </div>
    </div>
  );
}
