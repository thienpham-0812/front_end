import { Modal, message } from "antd";
import {
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
} from "@mui/material";
import { useState } from "react";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { Controller, useForm } from "react-hook-form";
import { useMutation, useQueryClient } from "react-query";
import classNames from "classnames";
import configs from "core/config";
import * as yup from "yup";

import { IAccountForm, IAccount } from "core/constants/interfaces";
import { createAccount, updateAccount } from "core/api/account";
import { ActionType } from "core/constants/enums";
import UploadImage from "core/components/UploadImage";
import { uploadImage } from "core/api/upload";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import { yupResolver } from "@hookform/resolvers/yup";

interface IProps {
  open: boolean;
  onCancel: () => void;
  type: ActionType;
  data?: IAccount;
}

export default function AccountModal({ open, onCancel, type, data }: IProps) {
  const queryClient = useQueryClient();
  const {
    handleSubmit,
    control,
    watch,
    formState: { errors },
  } = useForm<IAccountForm>({
    resolver: yupResolver(
      yup
        .object({
          email: yup
            .string()
            .email("Vui lòng nhập đúng định dạng email")
            .required("Đây là trường bắt buộc"),
          password:
            type === ActionType.CREATE
              ? yup
                  .string()
                  .min(6, "Vui lòng đặt mật khẩu lớn hơn 6 kí tự")
                  .required("Đây là trường bắt buộc")
              : yup.string(),
          password_confirm:
            type === ActionType.CREATE
              ? yup
                  .string()
                  .min(6, "Vui lòng đặt mật khẩu lớn hơn 6 kí tự")
                  .required("Đây là trường bắt buộc")
                  .test(
                    "validateConfirmPassword",
                    "Mật khẩu được nhập lại chưa đúng",
                    (value?: any) => {
                      return (watch("password") as string) === value;
                    }
                  )
              : yup.string(),
          name: yup.string().nullable().required("Đây là trường bắt buộc"),
        })
        .required()
    ),
    mode: "onChange",
    defaultValues:
      type === ActionType.CREATE
        ? {
            email: "",
            password: "",
            name: "",
          }
        : {
            email: data?.email,
            name: data?.name,
          },
  });

  const [showPassword, setShowPassword] = useState(false);

  const { mutate: postAccount, isLoading: loadingPostAccount } = useMutation(
    (payload: any) => createAccount(payload),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("accounts");
        message.success("Tạo tài khoản thành công!");
        onCancel();
      },
    }
  );
  const { mutate: putAccount, isLoading: loadingPutAccount } = useMutation(
    (params: any) => updateAccount(params?.id, params?.payload),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("accounts");
        message.success("Cập nhật thông tin tài khoản thành công!");
        onCancel();
      },
    }
  );

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };
  const [showPassConfirm, setShowPassConfirm] = useState(false);
  const handleClickShowPasswordConfirm = () => {
    setShowPassConfirm(!showPassConfirm);
  };
  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };
  const onSubmit = async (dataSubmit: any) => {
    if (type === ActionType.CREATE) {
      postAccount(dataSubmit);
    } else {
      putAccount({
        id: data?.id,
        payload: dataSubmit,
      });
    }
  };

  return (
    <>
      <Modal
        title={
          type === ActionType.CREATE
            ? "Tạo tài khoản"
            : "Sửa thông tin tài khoản"
        }
        open={open}
        onCancel={onCancel}
        footer={
          <FooterModalSubmit
            onCancel={onCancel}
            onOk={handleSubmit(onSubmit)}
            isLoadingOnOk={
              type === ActionType.CREATE
                ? loadingPostAccount
                : loadingPutAccount
            }
          />
        }
        centered
        className="modal-custom"
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Controller
                name="name"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <>
                    <TextField
                      label="Họ tên"
                      className="w-100"
                      variant="outlined"
                      onChange={onChange}
                      value={value}
                      error={!!errors?.name?.message}
                      helperText={errors?.name?.message}
                    />
                  </>
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="email"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <>
                    <TextField
                      label="Email"
                      className={classNames("f-1 w-100", {
                        "mr-10": type === ActionType.CREATE,
                      })}
                      variant="outlined"
                      onChange={onChange}
                      value={value}
                      error={!!errors?.email?.message}
                      helperText={errors?.email?.message}
                    />
                  </>
                )}
              />
            </Grid>
            {type === ActionType.CREATE && (
              <Grid item xs={12}>
                <Controller
                  name="password"
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      sx={{ m: 1, width: "25ch" }}
                      variant="outlined"
                      className="f-1 m-0 w-100"
                    >
                      <InputLabel
                        htmlFor="outlined-adornment-password"
                        error={!!errors?.password?.message}
                      >
                        Mật khẩu
                      </InputLabel>
                      <OutlinedInput
                        id="outlined-adornment-password"
                        type={showPassword ? "text" : "password"}
                        value={value}
                        onChange={onChange}
                        error={!!errors?.password?.message}
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={handleClickShowPassword}
                              onMouseDown={handleMouseDownPassword}
                              edge="end"
                            >
                              {showPassword ? (
                                <VisibilityOff />
                              ) : (
                                <Visibility />
                              )}
                            </IconButton>
                          </InputAdornment>
                        }
                        label={"Mật khẩu"}
                      />
                      {!!errors?.password?.message && (
                        <FormHelperText error>
                          {errors?.password?.message}
                        </FormHelperText>
                      )}
                    </FormControl>
                  )}
                />
              </Grid>
            )}
            {type === ActionType.CREATE && (
              <Grid item xs={12}>
                <Controller
                  name="password_confirm"
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      sx={{ m: 1, width: "25ch" }}
                      variant="outlined"
                      className="f-1 m-0 w-100"
                    >
                      <InputLabel
                        htmlFor="outlined-adornment-password"
                        error={!!errors?.password_confirm?.message}
                      >
                        Nhập lại mật khẩu
                      </InputLabel>
                      <OutlinedInput
                        id="outlined-adornment-password"
                        type={showPassConfirm ? "text" : "password"}
                        value={value}
                        onChange={onChange}
                        error={!!errors?.password_confirm?.message}
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={handleClickShowPasswordConfirm}
                              onMouseDown={handleMouseDownPassword}
                              edge="end"
                            >
                              {showPassConfirm ? (
                                <VisibilityOff />
                              ) : (
                                <Visibility />
                              )}
                            </IconButton>
                          </InputAdornment>
                        }
                        label={"Nhập lại mật khẩu"}
                      />
                      {!!errors?.password_confirm?.message && (
                        <FormHelperText error>
                          {errors?.password_confirm?.message}
                        </FormHelperText>
                      )}
                    </FormControl>
                  )}
                />
              </Grid>
            )}
          </Grid>
        </form>
      </Modal>
    </>
  );
}
