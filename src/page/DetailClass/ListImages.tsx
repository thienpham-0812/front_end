import { Grid } from "@mui/material";
import { message } from "antd";
import { apiDeleteImage } from "core/api/common";
import { apiGetDataImagePicked } from "core/api/download";
import { apiUploadListImage } from "core/api/upload";
import configs from "core/config";
import useWindowDimensions from "core/hooks/common/useWindowDimensions";
import { useEffect, useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { useParams } from "react-router-dom";
import styled from "styled-components";
import ModalEditImage from "./ModalEditImg";

const ImageContainer = styled.div`
  position: relative;
  cursor: pointer;
`;

const ImageOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 0.6),
    rgba(0, 0, 0, 0.4)
  );
  opacity: 0.8;
  border-radius: 4px;
`;

const ImageGridItem = styled.img`
  aspect-ratio: 1 / 1;
  object-fit: cover;
  border-radius: 4px;
`;

const IMAGES_PER_ROW = 5;
const GAP = 2;
const GAP_SPACE = (IMAGES_PER_ROW - 1) * (GAP * 8);

const ListImages = ({
  list,
  listPathImageDelete,
  setListPathImageDelete,
  isDeleteMode,
}: any) => {
  const { id } = useParams();
  const queryClient = useQueryClient();
  const { width } = useWindowDimensions();
  const [imagePicked, setImagePicked] = useState<any>();
  const [isOpenModalEditImage, setOpenModalEditImage] = useState(false);
  const [dataImagePicked, setDataImagePicked] = useState<string>("");

  const { mutate: getDataImagePicked, isLoading: isLoadingDownload } =
    useMutation(
      () =>
        apiGetDataImagePicked(
          imagePicked?.image.replace(configs.API_DOMAIN.replace("api", ""), "")
        ),
      {
        onSuccess: (data) => {
          setDataImagePicked(URL.createObjectURL(data));
        },
      }
    );

  useEffect(() => {
    if (imagePicked) {
      setDataImagePicked("");
      getDataImagePicked();
    }
  }, [imagePicked]);

  const { mutate: uploadImage, isLoading: isUploading } = useMutation(
    (payload: any) => apiUploadListImage(payload),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("detailClass");
        message.success("Upload ảnh thành công!");
        setImagePicked(null);
        setOpenModalEditImage(false);
      },
    }
  );

  const [loadingDelete, setLoadingDelete] = useState(false);
  const handleEditImage = async (newImageFile: any, imageInfo: any) => {
    try {
      setLoadingDelete(true);
      const resultDelete = await apiDeleteImage(
        imagePicked.image.replace(configs.API_DOMAIN.replace("api", ""), "")
      );
      setLoadingDelete(false);
      if (resultDelete.success) {
        uploadImage({ ...imageInfo, image: newImageFile, class_id: id });
      }
    } catch (error) {
    } finally {
      setLoadingDelete(false);
    }
  };

  const { mutate: uploadImageClone, isLoading: isUploadingClone } = useMutation(
    (payload: any) => apiUploadListImage(payload),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("detailClass");
        message.success("Lưu bản sao thành công!");
      },
    }
  );

  const handleSaveAs = async (newImageFile: any, imageInfo: any) => {
    try {
      uploadImageClone({ ...imageInfo, image: newImageFile, class_id: id });
    } catch (error) {
    } finally {
    }
  };

  const handleTickImageDelete = (path: any) => {
    if (listPathImageDelete.includes(path)) {
      setListPathImageDelete(
        listPathImageDelete.filter((item: any) => item !== path)
      );
    } else {
      setListPathImageDelete([path, ...listPathImageDelete]);
    }
  };

  return (
    <Grid container spacing={GAP}>
      {list?.map((item: any, index: number) => (
        <Grid item xs={12 / IMAGES_PER_ROW} key={index}>
          <ImageContainer
            onClick={() => {
              if (isDeleteMode) {
                handleTickImageDelete(item.image);
                return;
              }
              setImagePicked(item);
              setOpenModalEditImage(true);
            }}
          >
            <ImageGridItem
              id={`img-${index}`}
              src={configs.API_DOMAIN.replace("api", "") + item.image}
              alt={item.image}
              width={(width - GAP_SPACE) / IMAGES_PER_ROW}
            />
            {isDeleteMode && listPathImageDelete.includes(item.image) && (
              <ImageOverlay />
            )}
          </ImageContainer>
        </Grid>
      ))}
      {isOpenModalEditImage && (
        <ModalEditImage
          saveImage={handleEditImage}
          saveAs={handleSaveAs}
          image={imagePicked}
          imageUrlRoot={dataImagePicked}
          isOpen={isOpenModalEditImage}
          onCancel={() => {
            setOpenModalEditImage(false);
          }}
          isLoadingImage={isLoadingDownload || loadingDelete || isUploading}
          isLoadingClone={isUploadingClone}
        />
      )}
    </Grid>
  );
};

export default ListImages;
