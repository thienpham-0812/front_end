import { Grid } from "@mui/material";
import { Modal, message } from "antd";
import { apiDeleteImage } from "core/api/common";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import configs from "core/config";
import { isEmpty } from "lodash";
import { useEffect, useState } from "react";
import { useQueryClient } from "react-query";
import styled from "styled-components";

const ImageThumbnail = styled.img`
  object-fit: cover;
  aspect-ratio: 1 / 1;
  width: 100%;
  border: 2px solid black;
  border-radius: 5px;
`;

const IconRemove = styled.div`
  position: absolute;
  cursor: pointer;
  top: 0;
  right: -5px;
  padding-left: 4px;
  padding-right: 4px;
  border-radius: 1000px;
  background-color: #ff6969;
  border: 2px solid white;
  color: black;
  :hover {
    color: white;
    transform: scale(1.2);
  }
`;

interface IProps {
  isOpen: boolean;
  onCancel: any;
  setListPathImageDelete: any;
  setIsDeleteMode: any;
  listPathImageDelete: any[];
}
const ModalDeleteImage = (props: IProps) => {
  const { isOpen, onCancel, setListPathImageDelete, listPathImageDelete, setIsDeleteMode } =
    props;
  const queryClient = useQueryClient();

  const [isLoadingDelete, setLoadingDelete] = useState(false);
  const handleDeleteListImage = async () => {
    try {
      setLoadingDelete(true);
      const resultDelete = await apiDeleteImage(listPathImageDelete);
      if (resultDelete.success) {
        queryClient.invalidateQueries("detailClass");
        message.success("Đã xóa thành công các ảnh đã chọn");
        setIsDeleteMode(false);
        onCancel();
      }
    } catch (e) {
    } finally {
      setLoadingDelete(false);
    }
  };

  useEffect(() => {
    if (isEmpty(listPathImageDelete)) {
      onCancel();
    }
  }, [listPathImageDelete]);

  return (
    <Modal
      title="Xác nhận danh sách ảnh cần xóa"
      centered
      open={isOpen}
      onCancel={onCancel}
      className="modal-custom"
      footer={
        <FooterModalSubmit
          onOk={handleDeleteListImage}
          onCancel={onCancel}
          isLoadingOnOk={isLoadingDelete}
        />
      }
    >
      <Grid container spacing={1}>
        {listPathImageDelete?.map((path: string) => (
          <Grid item xs={4} position="relative">
            <IconRemove
              onClick={() => {
                setListPathImageDelete(
                  listPathImageDelete.filter((item: any) => item !== path)
                );
              }}
            >
              ｘ
            </IconRemove>
            <ImageThumbnail
              src={configs.API_DOMAIN.replace("api", "") + path}
              alt={path}
            />
          </Grid>
        ))}
      </Grid>
    </Modal>
  );
};

export default ModalDeleteImage;
