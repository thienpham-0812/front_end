import { yupResolver } from "@hookform/resolvers/yup";
import { Grid } from "@mui/material";
import { message, Modal } from "antd";
import { apiCreateClass, apiEditClass } from "core/api/common";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import CustomTextField from "core/components/ReactHookForm/TextField";
import { GET_LIST_CLASS } from "core/constants/queryName";
import { useForm } from "react-hook-form";
import { useMutation, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as yup from "yup";

interface IProps {
  title?: string;
  isOpen: boolean;
  onCancel: any;
  reFetchDataClass?: any;
  classInfo?: {
    class_name?: string;
    description?: string;
  };
}
const ModalEditClass = (props: IProps) => {
  const {
    classInfo,
    isOpen,
    onCancel,
    title = "Sửa thông tin class",
    reFetchDataClass,
  } = props;
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const { id } = useParams();

  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    defaultValues: classInfo,
    resolver: yupResolver(
      yup.object({
        class_name: yup.string().required("Đây là trường bắt buộc"),
        description: yup.string().required("Đây là trường bắt buộc"),
      })
    ),
  });

  const { mutate: editClass, isLoading: isLoadingEditClass } = useMutation(
    (data: any) => apiEditClass(id, data),
    {
      onSuccess: (data: any, variables: any) => {
        queryClient.invalidateQueries(GET_LIST_CLASS);
        message.success("Cập nhật thông tin class thành công!");
        reFetchDataClass();
        onCancel();
      },
    }
  );

  const { mutate: createClass, isLoading: isLoadingCreateClass } = useMutation(
    (payload: any) => apiCreateClass(payload),
    {
      onSuccess: (data: any, variables: any) => {
        queryClient.invalidateQueries(GET_LIST_CLASS);
        message.success("Tạo class thành công!");
        onCancel();
      },
    }
  );

  return (
    <Modal
      title={title}
      className="modal-custom"
      centered
      open={isOpen}
      onCancel={onCancel}
      footer={
        <FooterModalSubmit
          onOk={handleSubmit((values) => {
            if (title !== "Sửa thông tin class") createClass(values);
            else editClass(values);
          })}
          onCancel={onCancel}
          isLoadingOnOk={isLoadingEditClass || isLoadingCreateClass}
        />
      }
    >
      <Grid container gap={2}>
        <Grid item xs={12}>
          <CustomTextField
            name="class_name"
            control={control}
            label="Tên Class"
            errors={errors}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <CustomTextField
            name="description"
            control={control}
            label="Mô tả"
            required
            errors={errors}
            rows={8}
          />
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalEditClass;
