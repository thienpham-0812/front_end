import { Modal } from "antd";
import React from "react";
import "react-image-crop/src/ReactCrop.scss";

import FooterModalSubmit from "core/components/FooterModalSubmit";
import { useForm } from "react-hook-form";
import { Grid } from "@mui/material";
import CustomTextField from "core/components/ReactHookForm/TextField";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

interface ModalEditImageProps {
  isOpen: boolean;
  onCancel: any;
  image: any;
  setImageInfo: any;
  title: string;
  isLoading?: boolean;
}

enum TOOL {
  CURSOR = -1,
  CROP = 0,
  ROTATE = 2,
  ERASER = 3,
}

const ModalEditInfoImage: React.FC<ModalEditImageProps> = (props) => {
  const {
    title,
    isOpen,
    onCancel,
    image,
    setImageInfo,
    isLoading = false,
  } = props;

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      name: image.name,
      description: image.description,
    },
    resolver: yupResolver(
      yup.object({
        name: yup.string().required("Đây là trường bắt buộc"),
        description: yup.string().required("Đây là trường bắt buộc"),
      })
    ),
  });

  const onSubmit = async (data: any) => {
    await setImageInfo(data);
    await onCancel();
    await reset();
  };

  return (
    <Modal
      title={title}
      open={isOpen}
      onCancel={onCancel}
      className="modal-custom"
      centered
      footer={
        <FooterModalSubmit
          onCancel={onCancel}
          onOk={handleSubmit(onSubmit)}
          isLoadingOnOk={isLoading}
        />
      }
    >
      <Grid container gap={3}>
        <Grid item xs={12}>
          <CustomTextField
            name="name"
            control={control}
            label="Đặt tên file ảnh"
            errors={errors}
            required
          />
        </Grid>
        <Grid item xs={12} marginBottom={1}>
          <CustomTextField
            name="description"
            control={control}
            label="Mô tả ảnh"
            errors={errors}
            rows={8}
            required
          />
        </Grid>
      </Grid>
    </Modal>
  );
};

export default ModalEditInfoImage;
