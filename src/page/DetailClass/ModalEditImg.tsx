import { Button, Modal, Popover, Slider, Spin, message } from "antd";
import React, {
  useRef,
  useEffect,
  useState,
  MouseEvent,
  useCallback,
} from "react";
import "react-image-crop/src/ReactCrop.scss";

import Draggable from "react-draggable";
import ReactCrop, { Crop, PixelCrop } from "react-image-crop";

import styled from "styled-components";
import { urlToFile } from "core/helpers";
import { images } from "core/assets";
import classNames from "classnames";
import styles from "./styles.module.scss";
import ModalEditInfoImage from "./ModalEditInfoImg";
import ModalConfirm from "core/components/ModalConfirm";
import axios from "axios";

const ToolContainer = styled.div`
  background-color: #222433;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 50px;
  min-width: 50%;
  padding: 0 70px 0 30px;
  margin: 0 auto;
`;

const ToolEditor = styled.div`
  min-height: 50px;
`;

const ImageContainer = styled.div`
  width: 80vw;
  height: 80vh;
  overflow: scroll;
`;

const InfoContainer = styled.div`
  display: flex;
  align-items: center;
  padding-left: 30px;
  padding-right: 30px;
  padding-top: 10px;
`;

const NameImage = styled.div`
  font-size: 16px;
  font-weight: 700;
  color: #222433;
`;

const IconPointer = styled.img`
  cursor: pointer;
`;

const ToolCropContainer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  justify-content: space-around;
  margin-top: 20px;
`;

const SwitchContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 20px;
  overflow: hidden;
  box-shadow: 0px 0px 39px -3px rgba(0, 0, 0, 0.49);
  -webkit-box-shadow: 0px 0px 39px -3px rgba(0, 0, 0, 0.49);
  -moz-box-shadow: 0px 0px 39px -3px rgba(0, 0, 0, 0.49);
`;

const SidePart = styled.div<any>`
  background-color: ${(props: any) => (props.isActive ? "#ff6b00" : "#e6c9b3")};
  flex: 1;
  text-align: center;
  width: 60px;
  height: 40px;
  cursor: pointer;
  align-items: center;
  display: flex;
  justify-content: center;
`;

const ButtonIconTool = ({
  iconSrc,
  hoverContent,
  onClick,
  isActive,
  disabled,
}: any) => {
  return (
    <Popover placement="bottom" content={hoverContent}>
      <div
        onClick={() => {
          if (disabled) return;
          onClick();
        }}
        className={classNames("d-flex-c-c btn-tool-icon", {
          [styles.activeIcon]: isActive,
        })}
      >
        <img src={iconSrc} alt={hoverContent} />
      </div>
    </Popover>
  );
};

interface ModalEditImageProps {
  imageUrlRoot: string;
  isOpen: boolean;
  onCancel: any;
  isLoadingImage?: boolean;
  isLoadingClone?: boolean;
  saveImage: (imgData: any, info?: any) => void;
  image?: any;
  saveAs?: any;
}

enum TOOL {
  CURSOR = -1,
  CROP = 0,
  ROTATE = 2,
  ERASER = 3,
}

const API_REMOVE_KEY = "vdGdfbLH8wZrHiVHqFKYNedK";

const ModalEditImage: React.FC<ModalEditImageProps> = (props) => {
  const {
    imageUrlRoot,
    isOpen,
    onCancel,
    isLoadingImage = false,
    saveImage,
    image,
    saveAs,
    isLoadingClone,
  } = props;
  const [imageUrl, setImageUrl] = useState(imageUrlRoot);
  const [imageInfo, setImageInfo] = useState(image);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [ctx, setCtx] = useState<CanvasRenderingContext2D | null>(null);
  const [rotation, setRotation] = useState(0);
  const [zoom, setZoom] = useState(1);
  const [toolEditCurrent, setToolEditCurrent] = useState(TOOL.CURSOR);
  const [crop, setCrop] = useState<Crop>();
  const [completedCrop, setCompletedCrop] = useState<PixelCrop>();
  const [isCropCircle, setIsCropCircle] = useState(false);
  const [openModalEditDesc, setOpenModalEditDesc] = useState(false);
  const [isEdited, setIsEdited] = useState(false);

  useEffect(() => {
    setImageUrl(imageUrlRoot);
  }, [imageUrlRoot]);

  const getLengthDiagonalLineOfRectangle = (width: number, height: number) => {
    return Math.sqrt(width * width + height * height);
  };

  const drawImage = useCallback(() => {
    if (canvasRef.current && ctx) {
      const image = new Image();
      image.src = imageUrl;
      image.onload = () => {
        if (canvasRef.current) {
          let newSizeCanvas = getLengthDiagonalLineOfRectangle(
            image.width * zoom,
            image.height * zoom
          );
          if (image.width === image.height) newSizeCanvas = image.width * zoom;
          canvasRef.current.width = canvasRef.current.height = newSizeCanvas;
          ctx.save();
          ctx.clearRect(
            0,
            0,
            canvasRef.current.width,
            canvasRef.current.height
          );
          ctx.translate(
            canvasRef.current.width / 2,
            canvasRef.current.height / 2
          );
          ctx.rotate((rotation * Math.PI) / 180);
          ctx.scale(zoom, zoom);
          ctx.drawImage(
            image,
            -image.width / 2,
            -image.height / 2,
            image.width,
            image.height
          );
          ctx.restore();
        }
      };
    }
  }, [ctx, imageUrl, rotation, zoom]);

  useEffect(() => {
    if (canvasRef.current) {
      const canvasContext = canvasRef.current.getContext("2d");
      setCtx(canvasContext);
    }
  }, []);

  useEffect(() => {
    drawImage();
  }, [drawImage, imageUrl]);

  const resetTool = () => {
    setCrop(undefined);
    setCompletedCrop(undefined);
    setZoom(1);
    setRotation(0);
  };

  const cropCircular = () => {
    const canvas = canvasRef.current;
    if (!canvas || !ctx || !completedCrop) return;
    const image = new Image();
    image.src = canvas.toDataURL("image/png");
    image.onload = () => {
      const { width, height, x, y } = completedCrop;
      canvas.width = width;
      canvas.height = height;
      ctx.beginPath();
      const centerX = width / 2;
      const centerY = height / 2;
      const radiusX = width / 2;
      const radiusY = height / 2;
      ctx.ellipse(centerX, centerY, radiusX, radiusY, 0, 0, 2 * Math.PI);
      ctx.closePath();
      ctx.clip();
      ctx.drawImage(image, x, y, width, height, 0, 0, width, height);
      ctx.restore();
      setImageUrl(canvas.toDataURL("image/png"));
      resetTool();
    };
  };

  const cropRectangle = () => {
    const canvas = canvasRef.current;
    if (!canvas || !ctx || !completedCrop) return;
    const image = new Image();
    image.src = canvas.toDataURL("image/png");
    image.onload = () => {
      const { width, height, x, y } = completedCrop;
      canvas.width = width;
      canvas.height = height;
      ctx.drawImage(image, x, y, width, height, 0, 0, width, height);
      ctx.restore();
      setImageUrl(canvas.toDataURL("image/png"));
      resetTool();
    };
  };

  const handleSaveCrop = () => {
    setIsEdited(true);
    if (isCropCircle) cropCircular();
    else cropRectangle();
  };

  const handleStartingEraser = (e: MouseEvent<HTMLCanvasElement>) => {
    if (!ctx || !canvasRef.current) return;

    const rect = canvasRef.current.getBoundingClientRect();
    ctx.globalCompositeOperation = "destination-out";
    ctx.beginPath();
    ctx.arc(
      e.clientX - rect.left,
      e.clientY - rect.top,
      10,
      0,
      Math.PI * 2,
      true
    );
    ctx.fill();
    setIsEdited(true);
  };

  const handleDrag = (e: MouseEvent<HTMLCanvasElement>) => {};

  const handleMouseDown = (e: MouseEvent<HTMLCanvasElement>) => {
    if (toolEditCurrent === TOOL.ERASER) handleStartingEraser(e);
    else if (toolEditCurrent === TOOL.CURSOR) handleDrag(e);
  };

  const handleDownload = () => {
    if (canvasRef.current) {
      const link = document.createElement("a");
      link.href = canvasRef.current.toDataURL("image/png");
      link.download = (imageInfo?.name ?? "image") + ".png";
      link.click();
      setToolEditCurrent(TOOL.CURSOR);
    }
  };

  const handleRotate = (degree: number) => {
    setRotation(degree);
    setIsEdited(true);
  };

  const handleZoom = (factor: number) => {
    setZoom((prevZoom) => prevZoom * factor);
    setToolEditCurrent(TOOL.CURSOR);
    setIsEdited(true);
  };

  useEffect(() => {
    setCrop(undefined);
  }, [toolEditCurrent]);

  const handleSaveImage = async () => {
    urlToFile(
      canvasRef.current?.toDataURL(),
      (imageInfo?.name ?? "image") + ".png",
      "image/png"
    ).then((res) => {
      if (res.size / 1024 <= 10) {
        message.error("Kích thước ảnh quá nhỏ, vui lòng tăng kích thước ảnh");
        return;
      }
      if (res.size / 1024 > 1024) {
        message.error("Kích thước ảnh quá lớn, vui lòng giảm kích thước ảnh");
        return;
      }
      saveImage(res, imageInfo);
      resetTool();
    });
  };

  const [openModalCloneInfo, setOpenModalCloneInfo] = useState<any>();
  const handleSaveAs = (info: any) => {
    urlToFile(
      canvasRef.current?.toDataURL(),
      (info?.name ?? "image") + ".png",
      "image/png"
    ).then((res) => {
      saveAs(res, info);
    });
  };

  const [isOpenAlertEdited, setIsOpenAlertEdited] = useState(false);
  const handleCloseEditWindow = () => {
    if (isEdited) {
      setIsOpenAlertEdited(true);
    } else onCancel();
  };

  const [isLoadingAutoRemoveBg, setLoadingAutoRemoveBg] = useState(false);
  const getRemoveBgImage = async (formData: any) => {
    try {
      setLoadingAutoRemoveBg(true);
      const result = await axios.post(
        "https://api.remove.bg/v1.0/removebg",
        formData,
        {
          headers: {
            "X-Api-Key": API_REMOVE_KEY,
          },
          responseType: "blob",
        }
      );
      setImageUrl(URL.createObjectURL(result.data));
      setIsEdited(true);
    } catch (e) {
    } finally {
      setLoadingAutoRemoveBg(false);
    }
  };

  //vdGdfbLH8wZrHiVHqFKYNedK
  const handleRemoveBackgroundAuto = async () => {
    urlToFile(
      canvasRef.current?.toDataURL(),
      (imageInfo?.name ?? "image") + ".png",
      "image/png"
    ).then((res) => {
      const formData = new FormData();
      formData.append("size", "auto");
      formData.append("image_file", res);
      getRemoveBgImage(formData);
    });
  };

  return (
    <>
      <Modal
        open={isOpen}
        onCancel={handleCloseEditWindow}
        className="modal-preview-image"
        centered
        footer={null}
      >
        <ToolContainer>
          <ButtonIconTool
            iconSrc={images.cursor}
            hoverContent="Con trỏ"
            onClick={() => setToolEditCurrent(TOOL.CURSOR)}
            isActive={toolEditCurrent === TOOL.CURSOR}
          />
          <ButtonIconTool
            iconSrc={images.crop}
            hoverContent="Cắt ảnh"
            onClick={() => setToolEditCurrent(TOOL.CROP)}
            isActive={toolEditCurrent === TOOL.CROP}
          />
          <ButtonIconTool
            iconSrc={images.eraser}
            hoverContent="Xóa nền"
            onClick={() => setToolEditCurrent(TOOL.ERASER)}
            isActive={toolEditCurrent === TOOL.ERASER}
          />
          <ButtonIconTool
            iconSrc={images.autoRemoveBg}
            hoverContent="Xóa nền tự động"
            onClick={handleRemoveBackgroundAuto}
          />
          <ButtonIconTool
            iconSrc={images.rotate}
            hoverContent="Xoay ảnh"
            onClick={() => setToolEditCurrent(TOOL.ROTATE)}
            isActive={toolEditCurrent === TOOL.ROTATE}
          />
          <ButtonIconTool
            iconSrc={images.zoomOut}
            hoverContent="Phóng to"
            onClick={() => handleZoom(1.1)}
            // disabled={toolEditCurrent === TOOL.CROP}
          />
          <ButtonIconTool
            iconSrc={images.zoomIn}
            hoverContent="Thu nhỏ"
            onClick={() => handleZoom(1 / 1.1)}
            // disabled={toolEditCurrent === TOOL.CROP}
          />
          <ButtonIconTool
            iconSrc={images.downloadImage}
            hoverContent="Tải ảnh"
            onClick={handleDownload}
          />
          {!!saveAs && (
            <ButtonIconTool
              iconSrc={images.saveAsImage}
              hoverContent="Tạo bản sao"
              onClick={() => setOpenModalCloneInfo(true)}
            />
          )}
          <ButtonIconTool
            iconSrc={images.saveImage}
            hoverContent="Lưu ảnh"
            onClick={handleSaveImage}
          />
        </ToolContainer>
        <ToolEditor>
          {image && toolEditCurrent === TOOL.CURSOR && (
            <InfoContainer>
              <div>
                <IconPointer
                  src={images.editIcon}
                  alt="icon"
                  onClick={() => setOpenModalEditDesc(true)}
                />
              </div>
              <div className="ml-20">
                <NameImage>Tên ảnh: {imageInfo.name}</NameImage>
                <div>Mô tả: {imageInfo.description}</div>
              </div>
            </InfoContainer>
          )}

          {toolEditCurrent === TOOL.ROTATE && (
            <div className="w-100 d-flex-c-c">
              <Slider
                min={0}
                max={360}
                onChange={handleRotate}
                value={rotation}
                tooltip={{ formatter: (value?: number) => <>{`${value}°`}</> }}
                className="slider-custom"
              />
            </div>
          )}
          {toolEditCurrent === TOOL.CROP && (
            <ToolCropContainer>
              <SwitchContainer>
                <SidePart
                  isActive={isCropCircle}
                  onClick={() => setIsCropCircle(true)}
                >
                  <img
                    src={images.circleSwitch}
                    alt="circular"
                    style={{ width: "30px", height: "30px" }}
                  />
                </SidePart>
                <SidePart
                  isActive={!isCropCircle}
                  onClick={() => setIsCropCircle(false)}
                >
                  <img
                    src={images.rectangleSwitch}
                    alt="rectangle"
                    style={{ width: "23px", height: "23px" }}
                  />
                </SidePart>
              </SwitchContainer>
              <Button
                className="btn btn--create"
                onClick={handleSaveCrop}
                disabled={
                  !completedCrop ||
                  completedCrop?.width === 0 ||
                  completedCrop?.height === 0
                }
              >
                Crop
              </Button>
            </ToolCropContainer>
          )}
        </ToolEditor>
        <div className="d-flex-c-c" style={{ paddingTop: 10 }}>
          <ImageContainer
            className="d-flex-c-c"
            style={{
              backgroundImage: `url(${images.emptyBg})`,
              borderTop: "2px solid black",
            }}
          >
            {(isLoadingImage || !imageUrl || isLoadingAutoRemoveBg) && (
              <Spin size="large" delay={100} />
            )}
            <Draggable disabled={toolEditCurrent !== TOOL.CURSOR}>
              <div>
                <ReactCrop
                  crop={crop}
                  onChange={(_, percentCrop) => setCrop(percentCrop)}
                  onComplete={(c) => setCompletedCrop(c)}
                  circularCrop={isCropCircle}
                  ruleOfThirds
                  disabled={toolEditCurrent !== TOOL.CROP}
                  style={{
                    display:
                      isLoadingImage || isLoadingAutoRemoveBg
                        ? "none"
                        : "block",
                  }}
                >
                  <canvas
                    style={{ cursor: "pointer" }}
                    ref={canvasRef}
                    onMouseDown={handleMouseDown}
                    onMouseMove={(e) => {
                      if (e.buttons === 1) {
                        handleMouseDown(e);
                      }
                    }}
                  />
                </ReactCrop>
              </div>
            </Draggable>
          </ImageContainer>
        </div>
      </Modal>
      <ModalConfirm
        isOpen={isOpenAlertEdited}
        onCancel={() => setIsOpenAlertEdited(false)}
        content="Bạn có muốn lưu các thay đổi của ảnh không?"
        footer={
          <div className="d-flex-sr-c">
            <Button
              className="w-100 rounded-7"
              onClick={() => setIsOpenAlertEdited(false)}
            >
              Hủy
            </Button>
            <Button className="w-100 rounded-7" onClick={onCancel}>
              Không lưu
            </Button>
            <Button
              className="w-100 rounded-7"
              type="primary"
              onClick={() => {
                handleSaveImage();
                setIsOpenAlertEdited(false);
              }}
            >
              Lưu
            </Button>
          </div>
        }
      />
      {imageInfo && (
        <ModalEditInfoImage
          title="Sửa thông tin ảnh"
          isOpen={openModalEditDesc}
          onCancel={() => {
            setOpenModalEditDesc(false);
          }}
          image={imageInfo}
          setImageInfo={setImageInfo}
        />
      )}
      {!!saveAs && (
        <ModalEditInfoImage
          title="Điền thông tin cho bản sao"
          isOpen={openModalCloneInfo}
          onCancel={() => {
            setOpenModalCloneInfo(false);
          }}
          image={{}}
          setImageInfo={handleSaveAs}
          isLoading={isLoadingClone}
        />
      )}
    </>
  );
};

export default ModalEditImage;
