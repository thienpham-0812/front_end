import { Grid, InputAdornment, TextField } from "@mui/material";
import { Button, Empty, Spin, message } from "antd";
import { apiDeleteImage, apiGetDetailClass } from "core/api/common";
import { images } from "core/assets";
import configs from "core/config";
import { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import ListImages from "./ListImages";
import ModalEditClass from "./ModalEditClass";
import ModalUploadImage from "./ModalUploadImage";
import styles from "../ListClass/styles.module.scss";
import useFilter from "core/hooks/common/useFilter";
import ModalDeleteImage from "./ModalDeleteImage";

const NameClass = styled.div`
  font-size: 32px;
  font-weight: 600;
`;
const DescriptionContainer = styled.div`
  display: flex;
  align-items: center;
`;
const Description = styled.span`
  font-size: 16px;
  font-weight: 400;
  font-style: italic;
  color: #818181;
  margin-left: 10px;
`;

const IconPointer = styled.img`
  cursor: pointer;
`;

const AlertText = styled.p`
  color: red;
  position: absolute;
  white-space: nowrap;
  top: -30px;
  left: 10px;
`;

const DetailClass = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [isOpenModalEditClass, setOpenModalEditClass] = useState(false);
  const [isOpenModalUploadImage, setOpenModalUploadImage] = useState(false);
  const [isDeleteMode, setIsDeleteMode] = useState(false);
  const [listPathImageDelete, setListPathImageDelete] = useState<string[]>([]);

  useEffect(() => {
    setListPathImageDelete([]);
  }, [isDeleteMode]);

  const { filter, handleSearch } = useFilter({});

  const {
    data: detailClassApi,
    isLoading: loadingGetDetailClass,
    refetch: reFetchDataClass,
  } = useQuery(["detailClass", filter], () => {
    if (!id) navigate("/list-class");
    else return apiGetDetailClass(id, { key_word: filter.keyword });
  });

  const [detailClass, setDetailClass] = useState<any>();

  useEffect(() => {
    if (detailClassApi) setDetailClass(detailClassApi);
  }, [detailClassApi]);

  useEffect(() => {
    reFetchDataClass();
  }, [id]);

  const [isOpenConfirmDelete, setIsOpenConfirmDelete] = useState(false);

  return (
    <Grid container>
      <Grid item container xs={12} borderBottom={3} pb={2} borderColor="gray">
        <Grid item xs={5.5}>
          <NameClass>{detailClass?.data?.class_name}</NameClass>
          <DescriptionContainer>
            <IconPointer
              src={images.editIcon}
              alt="icon"
              onClick={() => setOpenModalEditClass(true)}
            />
            <Description>{detailClass?.data?.description}</Description>
          </DescriptionContainer>
        </Grid>
        <Grid
          item
          xs={6.5}
          container
          justifyContent="right"
          alignItems="center"
          columnGap={3}
        >
          {!isDeleteMode && (
            <>
              <Grid item>
                <TextField
                  placeholder="Tìm kiếm ..."
                  className={styles.searchBar}
                  onChange={handleSearch.keywordSearch}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <img src={images.searchIcon} />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <Button
                  className="btn btn--deny"
                  onClick={() => setIsDeleteMode(true)}
                >
                  Xóa ảnh -
                </Button>
              </Grid>
              <Grid item>
                <Button
                  className="btn btn--accept"
                  onClick={() => setOpenModalUploadImage(true)}
                >
                  Thêm ảnh +
                </Button>
              </Grid>
            </>
          )}
          {isDeleteMode && (
            <>
              <Grid item position="relative">
                <Button
                  className="btn btn--deny"
                  onClick={() => setIsOpenConfirmDelete(true)}
                >
                  Xác nhận xóa
                </Button>
                <AlertText>Click vào những ảnh cần xóa</AlertText>
              </Grid>
              <Grid item>
                <Button
                  className="btn btn--create"
                  onClick={() => setIsDeleteMode(false)}
                >
                  Hủy xóa ảnh
                </Button>
              </Grid>
            </>
          )}
        </Grid>
      </Grid>
      <Grid item container paddingY={2}>
        {!loadingGetDetailClass && (
          <ListImages
            list={detailClass?.data?.images}
            listPathImageDelete={listPathImageDelete}
            setListPathImageDelete={setListPathImageDelete}
            isDeleteMode={isDeleteMode}
          />
        )}
        {loadingGetDetailClass && (
          <div className="w-100 d-flex-c-c" style={{ height: "350px" }}>
            <Spin size="large" delay={100} />
          </div>
        )}
        {!loadingGetDetailClass && !detailClass?.data?.images?.length && (
          <Empty className="w-100 mt-20" />
        )}
      </Grid>
      <ModalDeleteImage
        isOpen={isOpenConfirmDelete}
        onCancel={() => setIsOpenConfirmDelete(false)}
        listPathImageDelete={listPathImageDelete}
        setListPathImageDelete={setListPathImageDelete}
        setIsDeleteMode={setIsDeleteMode}
      />
      {isOpenModalUploadImage && (
        <ModalUploadImage
          isOpen={isOpenModalUploadImage}
          onCancel={() => setOpenModalUploadImage(false)}
        />
      )}

      {!loadingGetDetailClass && isOpenModalEditClass && (
        <ModalEditClass
          isOpen={isOpenModalEditClass}
          reFetchDataClass={reFetchDataClass}
          onCancel={() => setOpenModalEditClass(false)}
          classInfo={{
            class_name: detailClass?.data?.class_name,
            description: detailClass?.data?.description,
          }}
        />
      )}
    </Grid>
  );
};

export default DetailClass;
