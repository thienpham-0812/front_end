import { Grid } from "@mui/material";
import { Button, Empty, message, Modal } from "antd";
import { apiUploadListImage } from "core/api/upload";
import { images } from "core/assets";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import { useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { useParams } from "react-router-dom";
import styled from "styled-components";
import ModalEditImage from "./ModalEditImg";
import CustomTextField from "core/components/ReactHookForm/TextField";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

interface IProps {
  isOpen: boolean;
  onCancel: any;
}

const ImagePreview = styled.img`
  border-radius: 7px;
  aspect-ratio: 5 / 3;
  object-fit: contain;
  width: 100%;
  cursor: pointer;
  border: 1px solid black;
`;

const ModalUploadImage = (props: IProps) => {
  const { isOpen, onCancel } = props;
  const queryClient = useQueryClient();
  const { id } = useParams();
  const [payload, setPayload] = useState<any>({ class_id: id });
  const [isOpenModalEditImage, setOpenModalEditImage] = useState<any>({
    img: null,
    active: false,
    index: -1,
  });

  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    resolver: yupResolver(
      yup.object({
        name: yup.string().required("Đây là trường bắt buộc"),
        description: yup.string().required("Đây là trường bắt buộc"),
      })
    ),
  });

  const handleUpload = (event: any) => {
    setPayload({ ...payload, image: event.target.files[0] });
  };

  const handleButtonClick = () => {
    document.getElementById("fileInput")?.click();
  };
  const handleResetValueInput = () => {
    const fileInput: HTMLInputElement = document.getElementById(
      "fileInput"
    ) as HTMLInputElement;
    if (!fileInput) return;
    fileInput.value = "";
  };

  const { mutate: uploadImage, isLoading: isUploading } = useMutation(
    (payload: any) => apiUploadListImage(payload),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("detailClass");
        message.success("Upload ảnh thành công!");
        setPayload({ class_id: id });
        onCancel();
      },
    }
  );

  const onSubmit = (data: any) => {
    if (payload.image.size / 1024 <= 10) {
      message.error("Kích thước ảnh quá nhỏ, vui lòng tăng kích thước ảnh");
      return;
    }
    if (payload.image.size / 1024 > 1024) {
      message.error("Kích thước ảnh quá lớn, vui lòng giảm kích thước ảnh");
      return;
    }
    uploadImage({ ...data, ...payload });
  };

  return (
    <Modal
      title="Thêm ảnh"
      open={isOpen}
      onCancel={onCancel}
      className="modal-custom"
      centered
      footer={
        <FooterModalSubmit
          onCancel={onCancel}
          onOk={handleSubmit(onSubmit)}
          isLoadingOnOk={isUploading}
        />
      }
    >
      <Grid container>
        <Grid item xs={12} position="relative" pb={2}>
          <Button
            onClick={handleButtonClick}
            className="btn btn--primary w-100"
          >
            <img
              src={images.uploadImage}
              alt="upload-icon"
              width={20}
              className="mr-10"
            />
            Tải ảnh lên
          </Button>
          <input
            type="file"
            onChange={handleUpload}
            onClick={handleResetValueInput}
            hidden
            multiple
            id="fileInput"
            accept="image/*"
          />
        </Grid>
        <Grid item container xs={12} gap={2} maxHeight={400} overflow="auto">
          {payload?.image && (
            <>
              <Grid item xs={12} position="relative">
                <ImagePreview
                  src={URL.createObjectURL(payload?.image)}
                  alt="failed to load this image"
                  onClick={() => {
                    setOpenModalEditImage({
                      img: URL.createObjectURL(payload?.image),
                      active: true,
                    });
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <CustomTextField
                  name="name"
                  control={control}
                  label="Đặt tên file ảnh"
                  errors={errors}
                  required
                />
              </Grid>
              <Grid item xs={12} marginBottom={1}>
                <CustomTextField
                  name="description"
                  control={control}
                  label="Mô tả ảnh"
                  errors={errors}
                  rows={8}
                  required
                />
              </Grid>
            </>
          )}
          {!payload?.image && <Empty className="mt-20 mb-20 w-100" />}
        </Grid>
      </Grid>
      {isOpenModalEditImage.active && (
        <ModalEditImage
          isOpen={isOpenModalEditImage.active}
          onCancel={() => setOpenModalEditImage({ img: null, active: false })}
          imageUrlRoot={isOpenModalEditImage.img}
          saveImage={(data: any) => {
            setOpenModalEditImage({
              ...isOpenModalEditImage,
              img: URL.createObjectURL(data),
            });
            setPayload({ ...payload, image: data });
            setOpenModalEditImage({ img: null, active: false });
          }}
        />
      )}
    </Modal>
  );
};

export default ModalUploadImage;
