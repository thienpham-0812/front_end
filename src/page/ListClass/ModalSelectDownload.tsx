import { CheckOutlined } from "@ant-design/icons";
import { Grid } from "@mui/material";
import { Modal } from "antd";
import { apiDownloadListClass } from "core/api/download";
import { images } from "core/assets";
import FooterModalSubmit from "core/components/FooterModalSubmit";
import { useState } from "react";
import { useMutation } from "react-query";
import styled from "styled-components";

const RowClass = styled((props) => <Grid {...props} />)`
  cursor: pointer;
  background-color: ${(props) => (props.isSelected ? "#fabb0f3e" : "")};
  padding: 16px 32px !important;
  border-top: 1px solid #222433;
  position: relative;
  font-size: 16px;
  color: #222433;
  :last-child {
    border-bottom: 1px solid #222433;
  }
`;

const IconTicked = styled(CheckOutlined)`
  position: absolute;
  top: 0;
  right: 20px;
  color: #fea628 !important;
  font-size: 24px;
  transform: translate(0, 50%)
`;

interface IProps {
  isOpen: boolean;
  onCancel: () => void;
  listClass: any[];
}

const ModalSelectDownload = (props: IProps) => {
  const { isOpen, onCancel, listClass } = props;
  const [listIdClassDownload, setListIdClassDownload] = useState<any[]>([]);

  const { mutate: downloadListClass, isLoading: isDownloading } = useMutation(
    () => apiDownloadListClass(listIdClassDownload),
    {
      onSuccess: (data, variables, context) => {
        const link = document.createElement("a");
        link.href = URL.createObjectURL(data);
        link.setAttribute("download", "data_list_class.zip");
        link.click();
      },
    }
  );

  const handleDownload = async () => {
    downloadListClass();
  };

  const handleSelectClass = (item: any) => {
    if (listIdClassDownload.includes(item.id)) {
      setListIdClassDownload(
        listIdClassDownload.filter((id) => id !== item.id)
      );
    } else {
      setListIdClassDownload([...listIdClassDownload, item.id]);
    }
  };

  const handleSelectAll = () => {
    const listId = listClass.map((item) => item.id);
    setListIdClassDownload(listId);
  };

  return (
    <Modal
      title="Chọn danh sách class cần download"
      open={isOpen}
      onCancel={onCancel}
      className="modal-custom modal-download-class"
      centered
      footer={
        <FooterModalSubmit
          onCancel={onCancel}
          onOk={handleDownload}
          isLoadingOnOk={isDownloading}
        />
      }
    >
      <Grid container spacing={1}>
        {listClass.map((_class: any, index: number) => (
          <RowClass
            item
            xs={12}
            key={index}
            onClick={() => handleSelectClass(_class)}
            isSelected={listIdClassDownload.includes(_class.id)}
          >
            {_class.class_name}
            {listIdClassDownload.includes(_class.id) && <IconTicked />}
          </RowClass>
        ))}
      </Grid>
    </Modal>
  );
};

export default ModalSelectDownload;
