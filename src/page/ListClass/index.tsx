import {
  Grid,
  InputAdornment,
  Pagination,
  PaginationItem,
  TextField,
} from "@mui/material";
import { Carousel, Spin, Empty, Button, message } from "antd";
import { apiGetListClass } from "core/api/common";
import { GET_LIST_CLASS } from "core/constants/queryName";
import { useMutation, useQuery } from "react-query";
import BoxClassInfo from "./BoxClassInfo";
import styled from "styled-components";
import ModalEditClass from "page/DetailClass/ModalEditClass";
import { useState } from "react";
import styles from "./styles.module.scss";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { images } from "core/assets";
import useFilter from "core/hooks/common/useFilter";
import { IFilter } from "core/constants/interfaces";
import ModalSelectDownload from "./ModalSelectDownload";

const TotalClassTitle = styled(Grid)`
  font-size: 24px;
  font-weight: 500;
`;
const Total = styled.span`
  font-size: 32px;
  font-weight: 700;
`;

const CLASS_PER_PAGE = 9;
const defaultFilter: IFilter = {
  pageIndex: 1,
  pageSize: 10,
};

const ListClass = () => {
  const [isOpenModalCreateClass, setOpenModalCreateClass] = useState(false);
  const [isOpenModalDownload, setOpenModalDownload] = useState(false);
  const { filter, handleSearch } = useFilter(defaultFilter);
  const { data: listClass, isLoading: isLoadingGetListClass } = useQuery(
    [GET_LIST_CLASS, filter],
    () => apiGetListClass({ key_word: filter.keyword })
  );

  const getNumberImage = () => {
    if (!listClass?.data) return 0;
    return listClass?.data?.reduce(
      (sum: number, _class: any) => sum + _class.number_image,
      0
    );
  };

  return (
    <Grid container>
      <Grid
        item
        xs={12}
        container
        justifyContent="space-between"
        direction="row"
        pb={2}
        mb={3}
        borderBottom={2}
        borderColor="gray"
      >
        <Grid item container gap={10} maxWidth="max-content">
          <TotalClassTitle item>
            Số lượng class: <Total>{listClass?.data?.length}</Total>
          </TotalClassTitle>
          <TotalClassTitle item>
            Tổng số ảnh: <Total>{getNumberImage()}</Total>
          </TotalClassTitle>
        </Grid>
        <Grid item direction="row" display="flex" alignItems="center">
          <TextField
            placeholder="Tìm kiếm ..."
            className={styles.searchBar}
            onChange={handleSearch.keywordSearch}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <img src={images.searchIcon} />
                </InputAdornment>
              ),
            }}
          />
          <Button
            className="btn btn--create ml-20"
            onClick={() => setOpenModalCreateClass(true)}
          >
            Tạo Class +
          </Button>
          <Button
            className="btn btn--accept ml-20"
            onClick={() => {
              if (listClass?.data?.length === 0) {
                message.error("Chưa có class nào để download!");
                return;
              }
              setOpenModalDownload(true);
            }}
          >
            <img
              src={images.downloadImage}
              alt="upload-icon"
              width={20}
              className="mr-10"
            />
            Download
          </Button>
        </Grid>
      </Grid>
      {isLoadingGetListClass && <Spin />}
      {!isLoadingGetListClass && listClass?.data?.length !== 0 && (
        <>
          <Grid container spacing={1.5}>
            {listClass.data.map((_class: any, index: number) => (
              <Grid item xs={4} key={index}>
                <BoxClassInfo {..._class} />
              </Grid>
            ))}
          </Grid>
          {/* <Grid className="d-flex-c-c w-100" mt={2}>
            <Pagination count={10} variant="outlined" shape="rounded" />
          </Grid> */}
        </>
      )}

      {!isLoadingGetListClass && listClass.data.length === 0 && (
        <div className="d-flex-c-c w-100">
          <Empty />
        </div>
      )}

      <ModalEditClass
        title="Tạo Class"
        isOpen={isOpenModalCreateClass}
        onCancel={() => setOpenModalCreateClass(false)}
      />
      {listClass?.data.length > 0 && (
        <ModalSelectDownload
          isOpen={isOpenModalDownload}
          onCancel={() => setOpenModalDownload(false)}
          listClass={listClass?.data}
        />
      )}
    </Grid>
  );
};

export default ListClass;
