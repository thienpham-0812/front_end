import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { colors } from "core/assets";
import { useNavigate } from "react-router-dom";
import configs from "core/config";
import { apiDeleteClass } from "core/api/common";
import { Button, message } from "antd";
import ModalConfirm from "core/components/ModalConfirm";
import { useQueryClient } from "react-query";
import { GET_LIST_CLASS } from "core/constants/queryName";

const getColorByCount = (count: number) => {
  if (count === 0) return colors.newColor;
  if (count < 10) return colors.bigImageColor;
  return colors.haveImageColor;
};

const BoxContainer = styled((props: any) => <div {...props}></div>)`
  width: 100%;
  height: calc(33vh - 93px);
  border: 1px solid gray;
  border-top: ${(props) => `3px solid ${getColorByCount(props.count)}`};
  border-radius: 5px;
  padding: 15px;
  cursor: ${(props) => (props.waiting ? "wait" : "pointer")};
  background: #fff7ec;
  position: relative;
`;

const IconRemove = styled.div`
  position: absolute;
  cursor: pointer;
  top: -7px;
  right: -7px;
  padding: 2px 6px;
  border-radius: 1000px;
  background-color: #ff6969;
  border: 3px solid white;
  color: #fff7ec;
  :hover {
    color: black;
    transform: scale(1.2);
  }
`;

const NameRow = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const BoxName = styled.span`
  color: ${colors.titleForm};
  font-weight: 700;
  font-size: 16px;
`;

const Description = styled.div`
  color: ${colors.contentForm};
  font-weight: 700;
  font-size: 16px;
  text-overflow: ellipsis;
  overflow: hidden;
  word-wrap: break-word;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
`;

interface IProps {
  id: number;
  class_name: string;
  description?: string;
  number_image?: number;
}

const BoxClassInfo = (props: IProps) => {
  const { class_name, number_image, description, id } = props;
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const [isOpenConfirmDelete, setIsOpenConfirmDelete] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);
  const handleDeleteClass = async () => {
    try {
      setIsLoadingDelete(true);
      const result = await apiDeleteClass(id);
      if (result.success) {
        queryClient.invalidateQueries(GET_LIST_CLASS);
        setIsOpenConfirmDelete(false);
        message.success(`Xóa class "${class_name}" thành công!`);
      }
    } catch (err) {
    } finally {
      setIsLoadingDelete(false);
    }
  };

  return (
    <>
      <BoxContainer
        count={number_image}
        onClick={() => {
          if (isLoadingDelete) return;
          navigate(`detail/${id}`);
        }}
        waiting={isLoadingDelete}
      >
        <IconRemove
          onClick={(event: any) => {
            event.stopPropagation();
            setIsOpenConfirmDelete(true);
          }}
        >
          ｘ
        </IconRemove>
        <NameRow>
          <BoxName>{class_name}</BoxName>
        </NameRow>
        <div>{number_image + " ảnh"}</div>
        <hr className="mt-10 mb-10" />
        <Description>{description}</Description>
      </BoxContainer>
      <ModalConfirm
        isOpen={isOpenConfirmDelete}
        onCancel={() => setIsOpenConfirmDelete(false)}
        content={`Bạn có chắc chắn muốn xóa class "${class_name}" không `}
        footer={
          <>
            <div className="d-flex-sr-c">
              <Button
                className="w-100 rounded-7"
                onClick={() => setIsOpenConfirmDelete(false)}
              >
                Hủy
              </Button>
              <Button
                className="w-100 rounded-7"
                type="primary"
                onClick={handleDeleteClass}
              >
                Xóa
              </Button>
            </div>
          </>
        }
      />
    </>
  );
};

export default BoxClassInfo;
